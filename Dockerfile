FROM node:12.16.2-alpine3.10

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install

COPY . .

RUN npm run build

EXPOSE 9999

CMD [ "npm", "run", "pm2" ]
