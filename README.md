
# API

## Framework

**Technology Stack**

The API is built using Node.js written in Typescript.

**External Services**

The API interfaces with a single external service `Twitch API`

## Development 

**Prerequisites**

```
Node: ~v12.16.0
```

**Get Started**


1. Run `npm install` -  to get the required dependencies
2. Run `npm run start:dev` - this compiles typescript and watches for changes

## Test

The API has tests built using `Jest` and `Supertest`. Tests can be found within the /tests folder. To execute the tests run `npm run test`

## Build

The node application is written in typescript which is built and compiled to javascript with the following command `npm run build`

## Quality

A basic code quality check can be done by running `npm run lint`.

## Continuous Integration & Continuous Deployment
CI & CD are handled via Gitlab pipelines [here](https://gitlab.com/kaykhan/kcheckpoint/pipelines). The configuration for this can be found within the .gitlab-ci.yml file. There are 5 stages; security, quality, test, build. Typically there would be another 2 stages for deploy and regression. 

## Deployment (Local)

### Docker Compose

The API can be deployed locally via docker-compose

```
1. docker-compose build --no-cache
2. docker-compose up
```

### Kubernetes

The API can be deployed to a local version of kubernetes. It requires a local version of minikube and kubectl to be installed.

```
1. kubectl apply -f k8/local/namespace.yml
2. kubectl apply -f k8/local/service.yml
3. kubectl apply -f k8/local/deployment.yml

minikube service -n checkpoint checkpoint-api --url
```


## Documentation

Built using Swagger, documentation can be accessed via `/docs`

![Photo1](screenshots/screenshot1.png)

## Usage

![Photo2](screenshots/screenshot2.png)

![Photo3](screenshots/screenshot3.png)
