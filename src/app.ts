import express, { Application } from "express";
import morgan from "morgan";
import cors from "cors";
import helmet from "helmet";

import Config from "./config";
import ApplicationRouter from "./routes";

const App = () => {

    const app: Application = express();
    const config = Config();

    app.use(helmet());
    app.use(cors());
    app.use((morgan('combined')));
    app.use(express.json());
    app.set("port", config.port);
    app.use(ApplicationRouter());


    return app;

}





export default App;