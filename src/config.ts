import dotenv from "dotenv";

const Config = (): Config => {

    switch (process.env.NODE_ENV) {
        case "test":
            dotenv.config({path: '.env.test'});
            break;
        case "production":
            break;
        default:
            dotenv.config({path: '.env'});
    }

    const config = {
        env: process.env.NODE_ENV,
        port: process.env.PORT,
        twitch: {
            apiURL: process.env.TWITCH_API_URL,
            clientId: process.env.TWITCH_CLIENT_ID,
        }
    }

    return config;

}

interface Config {
    env?: string;
    port?: string;
    twitch: TwitchConfig;
}

interface TwitchConfig {
    apiURL: string | undefined;
    clientId: string | undefined;
}

export default Config;