import { Request, Response, NextFunction } from "express";
import { validationResult } from "express-validator";

const validationResultHandler = (req: Request, res: Response, next: NextFunction) => {

    const errors = validationResult(req);

    if (!errors.isEmpty()) {

        const errs = errors.array();

        const response = {
            errors: errs,
        };
        return res.status(422).json(response);

    }

    next();

};

export default validationResultHandler;
