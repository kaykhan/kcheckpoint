import { Request, Response } from "express";
import { Router } from "express";
import TwitchRouter from "./twitch/twitch.router";
import swaggerUi from "swagger-ui-express";
import swaggerDocument from "./docs/docs.json";

const ApplicationRouter = () => {

    const router: Router = Router();

    router.use("/api/twitch", TwitchRouter());

    router.get("/health", (req: Request, res: Response) => {
        return res.status(200).json({});
    });

    router.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
    
    return router;

}

export default ApplicationRouter;