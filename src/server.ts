import App from "./app";

const instance = App();

const Server = instance.listen(instance.get("port"), () => {
    console.log("Server Running On: 0.0.0.0:" + instance.get("port"));
});

export default Server;