import { Request, Response } from "express";
import TwitchService from "./twitch.service";

class TwitchController {

    twitchService: TwitchService;

    constructor() {
        this.twitchService = new TwitchService();
    }

    public get = async (req: Request, res: Response) => {

        const streamerId = req.params.streamerId;

        try {

            const streams = await this.twitchService.getStreamer(streamerId);

            const streamer = streams.data.data.filter((s: any) => s.user_name === streamerId).shift();

            if(!streamer) {

                return res.status(200).json({
                    streamer: streamerId,
                    online: false
                });

            }

            const games = await this.twitchService.getGame(streamer.game_id);
            const game = games.data.data.shift();


            return res.status(200).json({
                streamer:streamer.user_name,
                playing: game.name,
                online: true,
            });


        } catch (err) {
            return res.status(500).json({err: "Failed"});
        }

    }

 

}

export default TwitchController;