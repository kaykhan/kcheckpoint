import { Router } from "express";
import TwitchController from "./twitch.controller";
import TwitchValidator from "./twitch.validator";

const TwitchRouter = (): Router => {

    const router: Router = Router();
    const controller = new TwitchController();
    const validator = new TwitchValidator();

    router.get("/:streamerId", validator.get(), controller.get);

    return router;

}

export default TwitchRouter;