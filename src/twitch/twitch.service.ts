import Config from "./../config";
import axios, {AxiosRequestConfig, AxiosPromise} from "axios";


class TwitchService {

    config: Config;

    constructor() {
        this.config = Config();
    }

    public getStreamer = (streamer: string): AxiosPromise => {

        const endpoint = "helix/streams";
        const url = `${this.config.twitch.apiURL}/${endpoint}?user_login=${streamer}`;

        const requestConfig: AxiosRequestConfig = {
            url: url,
            method: "GET",
            headers: {
                "Client-ID": this.config.twitch.clientId,
            }
        }

        return axios(requestConfig);
    }

    public getGame = (gameId: string): AxiosPromise => {

        const endpoint = "helix/games";
        const url = `${this.config.twitch.apiURL}/${endpoint}?id=${gameId}`;

        const requestConfig: AxiosRequestConfig = {
            url: url,
            method: "GET",
            headers: {
                "Client-ID": this.config.twitch.clientId,
            }
        }

        return axios(requestConfig);

    }

}

export default TwitchService;