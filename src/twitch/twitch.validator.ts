import { body, param} from "express-validator";
import validationResultHandler from "./../middleware/validator.middleware";

class TwitchValidator {

    public get = () => [
        param("streamerId").exists({ checkNull: true }),
        validationResultHandler,
    ];

}

export default TwitchValidator;
