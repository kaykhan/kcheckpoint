import request from "supertest";
import server from "./../src/server";

describe("Application", () => {

    afterEach( async () => {
        await server.close()
    });

    it("should return 200 response on healthcheck", async () => {
        
        const response = await request(server).get("/health")
            .expect(200);
    })

})